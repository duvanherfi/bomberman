
#include <allegro.h>

//definicion de fila y columnas
#define MAXFILAS 17 //FILAS EJE Y
#define MAXCOL 27 //COLUMNAS EJE X

//nombre variables
BITMAP *buffer;
BITMAP *roca;
BITMAP *fondo1 ;//fondo blanco
BITMAP *fondo2 ;//inicio amarillo
BITMAP *fondo3 ;//nivel amarillo
BITMAP *fondo4 ;//salir amarillo
BITMAP *fondo5 ;//inicio blanco
BITMAP *cursor ;//cursor




//       [20-1]    [31-1]
char mapa[MAXFILAS][MAXCOL] = {

    "XXXXXXXXXXXXXXXXXXXXXXXXXX",
    "X                        X",
    "X X X X X X X X X X X X  X",
    "X                        X",
    "X X X X X X X X X X X X  X",
    "X                        X",
    "X X X X X X X X X X X X  X",
    "X                        X",
    "X X X X X X X X X X X X  X",
    "X                        X",
    "X X X X X X X X X X X X  X",
    "X                        X",
    "X X X X X X X X X X X X  X",
    "X                        X",
    "XXXXXXXXXXXXXXXXXXXXXXXXXX",


};

//funcion para hacer el mapa, solo crea el mundo, no lo muestra
void crear_mundo(){

    int row, col;

    for(row =0 ; row < MAXFILAS ; row++){

        for(col = 0; col < MAXCOL ; col++){

            if(mapa[row][col] == 'X'){
                draw_sprite(buffer, roca, col*30, row*30);
            }
        }
    }

}

//funcion dibujar mapa
void dibujar(){
     //fondo1=fondo5;
     blit(buffer,screen,0,0,0,0,800,530);
}



int main()
{
    //inicia los graficos en alegro
    allegro_init();
    //instalamos el raton
    install_mouse();
    //instalamos el TECLADO
    install_keyboard();

    //bits de los colores, 16, 32...
    set_color_depth(16);
    //detectar el controlador grafico del pc, Tama�o de la pantalla (800x530)
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, 800,449,0,0);
    //para crear la pantalla (300,200), se crea una variable tipo BITMAP
    buffer = create_bitmap(800,449);

    //cargar imagen del menu, como variables tipo BITMAP

    fondo1 = load_bitmap("FONDO1.bmp",NULL);//fondo blanco
    fondo2 = load_bitmap("FONDO2.bmp",NULL);//inicio amarillo
    fondo3 = load_bitmap("FONDO3.bmp",NULL);//nivel amarillo
    fondo4 = load_bitmap("FONDO4.bmp",NULL);//salir amarillo
    fondo5 = load_bitmap("FONDO5.bmp",NULL);//inicio blanco
    cursor = load_bitmap("cursor.bmp",NULL);//cursor
    roca = load_bitmap("muro.bmp",NULL);//roca, muro, no destructible

    bool salida = false;
    int c=0;

    while(!salida)
    {

        if(mouse_x > 327 && mouse_x < 469 && mouse_y > 148 && mouse_y < 193 && c==0)
        {

            //para imprimir el fondo2, se usa la funcion blit
            blit(fondo2,buffer,0,0,0,0,800,530);
            //si presiona click, 1:click izquierdo, 2:click derecho
            if(mouse_b & 1)
            {

                c=1;
                //fondo1=fondo5;

                //salida = true
                while(!key[KEY_ESC]){
                    blit(fondo5,buffer,0,0,0,0,800,530);
                    crear_mundo();
                    dibujar();
                }
                c=0;

            }
        }
        else if (mouse_x > 187 && mouse_x < 612 && mouse_y > 218 && mouse_y < 269 && c==0){
            //para imprimir el fondo3, se usa la funcion blit
            blit(fondo3,buffer,0,0,0,0,800,530);
                        if(mouse_b & 1)
            {
                salida = true;
            }
        }
        else if (mouse_x > 334 && mouse_x < 464 && mouse_y > 292 && mouse_y < 342 && c==0){
            //para imprimir el fondo4, se usa la funcion blit
            blit(fondo4,buffer,0,0,0,0,800,530);
                        if(mouse_b & 1)
            {
                salida = true;
            }
        }
        else blit(fondo1,buffer,0,0,0,0,800,530);

        //imprimir cursor pero con transparencia, masked_blit
        masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,13,22);

        //imprime el buffer en pantalla para que se vea todo el menu:
        blit(buffer,screen,0,0,0,0,800,530);

    }


    return 0;
}
END_OF_MAIN();
